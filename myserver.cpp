#include "myserver.h"
#include "smartconnect.h"
#include <QDataStream>

MyServer::MyServer(unsigned short int port)
{
    // Создаем объект TCP сервера
    this->server = new QTcpServer(this);
    // Начинаем прослушивать весь трафик (приходящий на хост по всем протокола
    // и со всех сетей) на определенном порту
    this->server->listen(QHostAddress::AnyIPv4, port);

    connect(server, SIGNAL(newConnection()), this, SLOT(newConnection()));

    qInfo() << "Server is started in port" << port;
}

void MyServer::newConnection()
{
    // Получаем сокет следующего соединения ожидающего в очереди на открытие
    QTcpSocket *newConnection = this->server->nextPendingConnection();
    QString ip = newConnection->peerAddress().toString();
    qInfo() << "New connection! IP:" << ip;
    this->connectionList[ip] = newConnection;
    // Подключаем обработчик новых поступающих данных
    connect(newConnection, SIGNAL(readyRead()), this, SLOT(newData()));
    // Подключаем обработчик закрытия соединения
    connect(newConnection, SIGNAL(disconnected()),
            newConnection, SLOT(deleteLater()));
    SmartConnect(newConnection, SIGNAL(disconnected), [=](){
        this->connectionList.remove(ip);
        newConnection->deleteLater();
    }, this);
}

void MyServer::newData()
{
    QTcpSocket* clientSocket = static_cast<QTcpSocket*>(this->sender());
    QDataStream inputStream;
    QString nickname;
    QString messageText;

    clientSocket->peerAddress();

    inputStream.setDevice(clientSocket);
    inputStream.setVersion(QDataStream::Qt_5_0);

    inputStream >> nickname >> messageText;

    qInfo() << nickname << messageText;

    foreach (auto key, this->connectionList.keys()) {
        this->sendMessage(nickname, messageText,
                          this->connectionList[key]);
    }

}

void MyServer::sendMessage(QString sender,
                           QString message,
                           QTcpSocket *connection)
{
    QByteArray outputBuffer;
    QDataStream outStream(&outputBuffer, QIODevice::WriteOnly);

    outStream << sender << message;
    qInfo() << "Send to:" << sender << message;

    connection->write(outputBuffer);
}
