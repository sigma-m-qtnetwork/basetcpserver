#ifndef MYSERVER_H
#define MYSERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMap>

class MyServer : public QObject
{
    Q_OBJECT
private:
    QTcpServer *server;
    QMap<QString, QTcpSocket*> connectionList;

public:
    explicit MyServer(unsigned short int port);

signals:

public slots:

private slots:
    void newConnection();
    void newData();
    void sendMessage(QString sender,
            QString message,
            QTcpSocket *connection);
};

#endif // MYSERVER_H
