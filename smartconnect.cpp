#include "smartconnect.h"

SmartConnect::SmartConnect(QObject *sender, const char *signal,
                           std::function<void (void)> callback, QObject *parent) : QObject (parent)
{
    // Сохранение указателя на функцию-слот
    this->callback = callback;

    connect(sender, signal, this, SLOT(voidSlot()));
}

void SmartConnect::voidSlot()
{
    this->callback();
}
