#ifndef SMARTCONNECT_H
#define SMARTCONNECT_H

#include <QObject>
#include <functional>

/**
 * @brief SmartConnect class - используется для соединения источника сигнала
 *          с обработчиком в виде лямбда выражения (анонимной функции) с или без
 *          замыканием
 */
class SmartConnect : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief SmartConnect - конструктор объекта который также выполняет коннект
     * @param sender - источник сигнала
     * @param signal - сам сигнал, передается как и в обычном connect с помощью макроса SIGNAL(signalName())
     *                  тип const char* используется потому что макрос SIGNAL() на самом деле преобразует
     *                  имя передаваемой функции в строку
     * @param callback - функция-слот которая будет вызвана при реакции на сигнал
     * @param parent - родительский класс
     */
    explicit SmartConnect(QObject *sender, const char *signal,
                          std::function<void()> callback, QObject *parent = nullptr);
private:
    /**
     * @brief callback - указатель на функцию-слот, необходим для её непосредственного вызова
     *                  внутри анонимного слота
     */
    std::function<void (void)> callback;
private slots:
    /**
     * @brief voidSlot - анонимный слот который будет связан через connect  с сигналом
     *                  и вызывающий внутри себя переданную функцию-слот
     */
    void voidSlot();
};

#endif // SMARTCONNECT_H
